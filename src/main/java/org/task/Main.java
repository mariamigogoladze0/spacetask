package org.task;

import org.apache.poi.ss.usermodel.*;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.*;

public class Main {
    public static void main(String[] args) {
        try {
            Workbook workbook = WorkbookFactory.create(Objects.requireNonNull(Main.class.getClassLoader().getResourceAsStream("Wings Developer Task 22.06.xlsx")));

            //hardcoded for input
            Sheet sheet = workbook.getSheetAt(0);
            Row transfersRow = sheet.getRow(13);
            Row datesRow = sheet.getRow(16);

            Iterator<Cell> transferIterator = transfersRow.iterator();
            Iterator<Cell> dateIterator = datesRow.iterator();

            Map<LocalDate, BigDecimal> dataMap = new HashMap<>();
            List<LocalDate> dates = new ArrayList<>();
            while (transferIterator.hasNext() && dateIterator.hasNext()) {
                double amountDouble = transferIterator.next().getNumericCellValue();
                BigDecimal amount = BigDecimal.valueOf(amountDouble).setScale(2, RoundingMode.HALF_EVEN);
                Date date = dateIterator.next().getDateCellValue();
                LocalDate localDate = date.toInstant()
                        .atZone(ZoneId.systemDefault())
                        .toLocalDate();
                localDate = localDate.withDayOfMonth(1);

                if (amountDouble < 300) {
                    continue;
                }

                dates.add(localDate);

                if (dataMap.get(localDate) == null) {
                    dataMap.put(localDate, amount);
                } else {
                    dataMap.put(localDate, dataMap.get(localDate).add(amount));
                }

            }
            dates = new ArrayList<>(new HashSet<>(dates));
            Collections.sort(dates);

            //1. Calculate Last 6 months average transfer from client's card   (return answer rounded to whole number)
            BigDecimal lastSixMonthTransferSum = BigDecimal.ZERO;
            for (int i = dates.size() - 1; i >= dates.size() - 6; --i) {
                lastSixMonthTransferSum = lastSixMonthTransferSum.add(dataMap.get(dates.get(i)));
            }
            int lastSixMonthTransferAvg = lastSixMonthTransferSum.divide(new BigDecimal(6), RoundingMode.HALF_EVEN).intValue();
            System.out.printf("lastSixMonthTransferAvg=%d \n", lastSixMonthTransferAvg);

            //2. Calculate maximum transferred money amount in one month. What was the month ?
            BigDecimal maxTransferMoney = BigDecimal.valueOf(Integer.MIN_VALUE);
            LocalDate maxDate = null;
            for (Map.Entry<LocalDate, BigDecimal> entry : dataMap.entrySet()) {
                LocalDate k = entry.getKey();
                BigDecimal v = entry.getValue();
                if (v.compareTo(maxTransferMoney) > 0) {
                    maxTransferMoney = v;
                    maxDate = k;
                }
            }
            System.out.printf("maxTransferMoney=%s date=%s \n", maxTransferMoney, maxDate);

            workbook.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}